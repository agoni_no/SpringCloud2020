package com.xunqi.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;

/**
 * @Created with IntelliJ IDEA.
 * @author: 夏沫止水
 * @create: 2020-04-07 12:02
 **/
@SpringBootApplication
@EnableHystrixDashboard
public class DashBoardHystrix9001 {

    public static void main(String[] args) {
        SpringApplication.run(DashBoardHystrix9001.class,args);
    }

}
