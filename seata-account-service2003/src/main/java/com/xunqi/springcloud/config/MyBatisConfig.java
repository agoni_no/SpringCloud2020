package com.xunqi.springcloud.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * @Description:
 * @Created with IntelliJ IDEA.
 * @author: 夏沫止水
 * @create: 2020-04-16 10:47
 **/
@Configuration
@MapperScan({"com.xunqi.springcloud.dao"})
public class MyBatisConfig {
}
