package com.xunqi.springcloud.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;

/**
 * @Description:
 * @Created with IntelliJ IDEA.
 * @author: 夏沫止水
 * @create: 2020-04-16 11:39
 **/
@Mapper
public interface AccountDao {

    /**
     * 减少用户余额
     * @param userId
     * @param money
     * @return
     */
    int decrease(@Param("userId") Long userId,
                 @Param("money") BigDecimal money);

}
