package com.xunqi.springcloud.service;

import java.math.BigDecimal;

/**
 * @Description:
 * @Created with IntelliJ IDEA.
 * @author: 夏沫止水
 * @create: 2020-04-16 11:44
 **/
public interface AccountService {

    /**
     * 减少用户余额
     * @param userId 用户id
     * @param money  用户余额
     */
    void decrease(Long userId, BigDecimal money);

}
