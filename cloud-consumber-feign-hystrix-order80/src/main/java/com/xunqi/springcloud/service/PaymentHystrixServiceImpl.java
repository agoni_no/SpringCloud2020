package com.xunqi.springcloud.service;

import org.springframework.stereotype.Component;

/**
 * @Created with IntelliJ IDEA.
 * @author: 夏沫止水
 * @create: 2020-04-06 21:32
 **/
@Component
public class PaymentHystrixServiceImpl implements PaymentHystrixService {
    @Override
    public String paymentInfo_OK(Integer id) {
        return "------ PaymentHystrixService fallback paymentInfo OK";
    }

    @Override
    public String paymentInfo_Timeout(Integer id) {
        return "------ PaymentHystrixService fallback paymentInfo timeout";
    }
}
