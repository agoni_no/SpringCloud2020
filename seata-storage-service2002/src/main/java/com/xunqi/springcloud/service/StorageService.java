package com.xunqi.springcloud.service;

/**
 * @Description:
 * @Created with IntelliJ IDEA.
 * @author: 夏沫止水
 * @create: 2020-04-16 11:21
 **/
public interface StorageService {

    /**
     * 减少商品库存
     * @param productId
     * @param count
     */
    void decrease(Long productId, Integer count);

}
