package com.xunqi.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @Created with IntelliJ IDEA.
 * @author: 夏沫止水
 * @create: 2020-04-13 11:27
 **/
@SpringBootApplication
@EnableDiscoveryClient
public class MainApp8401 {



    public static void main(String[] args) {
        SpringApplication.run(MainApp8401.class, args);
    }
}
