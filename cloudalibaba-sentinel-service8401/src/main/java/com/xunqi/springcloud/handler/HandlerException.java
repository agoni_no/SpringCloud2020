package com.xunqi.springcloud.handler;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.xunqi.springcloud.entities.CommonResult;
import com.xunqi.springcloud.entities.Payment;

/**
 * @Description:
 * @Created with IntelliJ IDEA.
 * @author: 夏沫止水
 * @create: 2020-04-14 11:10
 **/
public class HandlerException {

    public static CommonResult handlerException(BlockException exception) {

        return new CommonResult(444,"按客户自定义限流测试OK,global,HandlerException---1");
    }

    public static CommonResult handlerException2(BlockException exception) {

        return new CommonResult(444,"按客户自定义限流测试OK,global,HandlerException---2");
    }
}
