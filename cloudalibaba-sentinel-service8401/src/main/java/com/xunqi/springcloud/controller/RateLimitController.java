package com.xunqi.springcloud.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.xunqi.springcloud.entities.CommonResult;
import com.xunqi.springcloud.entities.Payment;
import com.xunqi.springcloud.handler.HandlerException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description:
 * @Created with IntelliJ IDEA.
 * @author: 夏沫止水
 * @create: 2020-04-14 10:51
 **/
@RestController
public class RateLimitController {

    @GetMapping("/byResource")
    @SentinelResource(value = "byResource",blockHandler = "handleException")
    public CommonResult byResource() {
        return new CommonResult(200,"按资源名称限流测试OK",new Payment(2020L,"serial001"));
    }


    public CommonResult handleException(BlockException exception) {
        return new CommonResult(444,exception.getClass().getCanonicalName() + "\t服务不可用");
    }


    @GetMapping("/rateLimit/byUrl")
    @SentinelResource(value = "byUrl")
    public CommonResult byUrl() {
        return new CommonResult(200,"按url限流测试OK",new Payment(2020L,"serial002"));
    }


    @GetMapping("/rateLimit/customerBlokHandler")
    @SentinelResource(value = "customerBlokHandler",
            blockHandlerClass = HandlerException.class,
            blockHandler = "handlerException2")
    public CommonResult customerBlokHandler() {
        return new CommonResult(200,"按客户自定义限流测试OK",new Payment(2020L,"serial003"));
    }

}
