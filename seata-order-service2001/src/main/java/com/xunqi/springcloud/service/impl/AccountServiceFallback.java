package com.xunqi.springcloud.service.impl;

import com.xunqi.springcloud.domain.CommonResult;
import com.xunqi.springcloud.service.AccountService;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

/**
 * @Description:
 * @Created with IntelliJ IDEA.
 * @author: 夏沫止水
 * @create: 2020-04-16 16:05
 **/
@Component
public class AccountServiceFallback implements AccountService {
    @Override
    public CommonResult decrease(Long userId, BigDecimal money) {
        return new CommonResult(4444,"响应超时");
    }
}
