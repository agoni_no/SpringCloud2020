package com.xunqi.springcloud.service;

import com.xunqi.springcloud.domain.Order;

/**
 * @Description:
 * @Created with IntelliJ IDEA.
 * @author: 夏沫止水
 * @create: 2020-04-16 10:20
 **/
public interface OrderService {

    /**
     * 新建订单
     * @param order
     * @return
     */
    void create(Order order);

}
