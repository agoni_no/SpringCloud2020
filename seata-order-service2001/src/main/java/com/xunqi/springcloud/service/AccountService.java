package com.xunqi.springcloud.service;

import com.xunqi.springcloud.domain.CommonResult;
import com.xunqi.springcloud.service.impl.AccountServiceFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;

/**
 * @Description:
 * @Created with IntelliJ IDEA.
 * @author: 夏沫止水
 * @create: 2020-04-16 10:28
 **/
// @Component
@FeignClient(value = "seata-account-service",fallback = AccountServiceFallback.class)
public interface AccountService {

    @PostMapping(value = "account/decrease")
    CommonResult decrease(@RequestParam("userId") Long userId,
                          @RequestParam("money") BigDecimal money);
}

