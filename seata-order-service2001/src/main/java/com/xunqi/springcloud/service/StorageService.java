package com.xunqi.springcloud.service;

import com.xunqi.springcloud.domain.CommonResult;
import com.xunqi.springcloud.service.impl.StorageServiceFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @Description:
 * @Created with IntelliJ IDEA.
 * @author: 夏沫止水
 * @create: 2020-04-16 10:28
 **/
// @Component
@FeignClient(value = "seata-storages-service",fallback = StorageServiceFallback.class)
public interface StorageService {

    @PostMapping("storage/decrease")
    CommonResult decrease(@RequestParam("productId") Long productId,
                          @RequestParam("count") Integer count);

}
