package com.xunqi.springcloud.service.impl;

import com.xunqi.springcloud.domain.CommonResult;
import com.xunqi.springcloud.service.StorageService;
import org.springframework.stereotype.Component;

/**
 * @Description:
 * @Created with IntelliJ IDEA.
 * @author: 夏沫止水
 * @create: 2020-04-16 15:58
 **/
@Component
public class StorageServiceFallback implements StorageService {
    @Override
    public CommonResult decrease(Long productId, Integer count) {
        return new CommonResult(4444,"响应超时");
    }
}
