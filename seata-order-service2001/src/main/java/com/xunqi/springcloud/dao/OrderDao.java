package com.xunqi.springcloud.dao;

import com.xunqi.springcloud.domain.Order;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Description:
 * @Created with IntelliJ IDEA.
 * @author: 夏沫止水
 * @create: 2020-04-16 10:09
 **/
@Mapper
public interface OrderDao {

    /**
     * 新建订单
     * @param order
     * @return
     */
    void create(Order order);


    /**
     * 修改订单，将状态从未支付改成已支付
     * @param userId
     * @param status
     * @return
     */
    void update(@Param("userId")Long userId,@Param("status")Integer status);
}
