package com.xunqi.springcloud.service;

/**
 * 发送消息接口
 * @Created with IntelliJ IDEA.
 * @author: 夏沫止水
 * @create: 2020-04-09 20:36
 **/
public interface IMessageProvider {

    /**
     * 发送消息
     * @return
     */
    public String send();

}
